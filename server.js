
const express = require('express')
const app = express()
const contacto = require('./contacto.json')
const fs = require('fs')
const path = require('path')

const hbs = require('hbs')
hbs.registerPartials(path.join(__dirname, 'views', 'partials'))
hbs.registerHelper('getCurrentYear', () => new Date().getFullYear())
// con paso de parámetro:
hbs.registerHelper('toUpperCase', text => text.toUpperCase())
app.set('view engine', 'hbs'); // clave valor

const staticRoute = path.join(__dirname, 'public')
app.use(express.static(staticRoute))

const staticRoute2 = path.join(__dirname, 'public2')
app.use('/static', express.static(staticRoute2))

app.use(function (req, res, next) {
    var now = new Date().toString()
    let log = `Time: ${now} ${req.method} ${req.url} \n`
    fs.appendFile("log.txt", log, (err) => {
        if (err) console.log(`No se ha podido usar el fichero de log:  ${err}`)
    })
    next()
})


app.use(function (req, res, next) {
    var now = new Date().toString()
    console.log(`Time: ${now} ${req.method} ${req.url}`)
    next()
})

app.get('/', (req, res) => {
    res.send('Hola Mundo!!!!')
})
app.get('/contactar', (req, res) => {
    res.render('contactar.hbs', {
        pageTitle: 'Contactar'
    })
})
app.get('/contacto', (req, res) => {
    res.send(contacto)
})
app.listen(3000, () => {
    console.log('Servidor web arrancado en el puerto 3000')
})
  